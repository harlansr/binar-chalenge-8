const express = require('express')
const app = express()
const swaggerUI = require('swagger-ui-express')
const swaggerJSON = require('./api_swagger.json')

// app.get('/', (req, res) => {
//   res.send('Chapter 8 - API Documentation')
// })

// Swagger docs
var options_swagger = {
  customCss: '.swagger-ui .topbar { display: none }',
  customSiteTitle: "API Documentation",
  customfavIcon: "/assets/image/logo-binar.png"
};

app.use('/', swaggerUI.serve, swaggerUI.setup(swaggerJSON, options_swagger))

// GET USER /user
app.put('/api/user/create', (req, res) => {
  res.send({
    id: 10,
    name: 'John',
    job: "Engineer"
  })
})

// POST USER /user
// PUT USER /user/:id


app.listen('3000', () => {
  console.log('App is running on : http://localhost:3000/')
})