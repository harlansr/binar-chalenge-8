// import logo from '../logo.svg';
import '../App.css';
import { Component } from "react";
import { Container, Card, Row } from 'react-bootstrap';
import { Form, FloatingLabel, Button } from 'react-bootstrap';
import CardPlayerTable from '../components/CardPlayerTable'

class Home extends Component {
    state = {
        players: [
            // {
            //     id: 1,
            //     username: 'HarlanSR',
            //     email: 'harlan@gmail.com',
            //     exp: 3200,
            //     lvl: 3,
            // },
            // {
            //     id: 2,
            //     username: 'Ashin',
            //     email: 'ashin@gmail.com',
            //     exp: 1240,
            //     lvl: 1,
            // },
            // {
            //     id: 3,
            //     username: 'Padi',
            //     email: 'padi@gmail.com',
            //     exp: 10240,
            //     lvl: 10,
            // },
            // {
            //     id: 4,
            //     username: 'Eki',
            //     email: 'eki@gmail.com',
            //     exp: 10240,
            //     lvl: 10,
            // },
        ],
        dataCreate: {
            username: '',
            email: '',
            password: '',
            exp: 0,
        },
        dataSearch: {
            username: '',
            email: '',
            exp: null,
            lvl: null,
        },
        username: '',
        email: '',
        password: '',
    }

    getPlayerData = async () => {
        let data_api = {}
        if (this.state.dataSearch.username) {
            data_api['username'] = this.state.dataSearch.username
        }
        if (this.state.dataSearch.email) {
            data_api['email'] = this.state.dataSearch.email
        }
        if (this.state.dataSearch.exp) {
            data_api['exp'] = this.state.dataSearch.exp
        }
        if (this.state.dataSearch.lvl) {
            data_api['lvl'] = this.state.dataSearch.lvl
        }

        const resp = await fetch('http://localhost:5000/api/players?' + new URLSearchParams(data_api))

        if (resp.status === 200) {
            const data = await resp.json()
            this.setState({
                players: data.message
            })
        } else {
            this.setState({
                players: []
            })
        }

    }

    componentDidMount() {
        this.getPlayerData()
    }

    handleSubmitCreate = async () => {
        const data = {
            username: this.state.dataCreate.username,
            email: this.state.dataCreate.email,
            password: this.state.dataCreate.password,
            exp: this.state.dataCreate.exp
        }

        if (!data.username || !data.email || !data.password) {
            console.log("Lengkapi data")
            return
        }

        const resp = await fetch(`http://localhost:5000/api/players`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })

        if (resp.status === 201) {
            // this.props.toggleFunc()
            this.getPlayerData()
            this.setState({
                dataCreate: {
                    username: '',
                    email: '',
                    password: '',
                    exp: 0,
                },
            })
        }
    }

    handleChange = async (event) => {
        await this.setState(data => ({
            dataCreate: {
                ...data.dataCreate,
                [event.target.id]: event.target.value
            }
        }))
    }

    handleChangeSearch = async (event) => {
        await this.setState(data => ({
            dataSearch: {
                ...data.dataSearch,
                [event.target.id]: event.target.value
            }
        }))
        this.getPlayerData()
    }

    render() {
        return (
            <Container fluid >
                <Row className="justify-content-center ">
                    <Card className="card-main my-5">
                        <Card.Body>
                            <Card.Title>KELOLA PLAYER</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">Tambahkan Player</Card.Subtitle>
                            <Card.Text>
                                Tambahkan player baru mengukan form dibawah.
                            </Card.Text>
                            <FloatingLabel
                                controlId="username"
                                label="Username"
                                className="mb-1"
                            >
                                <Form.Control
                                    type="text"
                                    placeholder="username"
                                    value={this.state.dataCreate.username}
                                    // id="dataCreate.username"
                                    onChange={this.handleChange} />
                            </FloatingLabel>
                            <FloatingLabel
                                controlId="email"
                                label="Email address"
                                className="mb-1"
                            >
                                <Form.Control
                                    type="email"
                                    placeholder="name@example.com"
                                    value={this.state.dataCreate.email}
                                    // id="dataCreate.email"
                                    onChange={this.handleChange} />
                            </FloatingLabel>
                            <FloatingLabel
                                controlId="password"
                                label="Password"
                                className="mb-1"
                            >
                                <Form.Control
                                    type="password"
                                    placeholder="password"
                                    value={this.state.dataCreate.password}
                                    // id="dataCreate.password"
                                    onChange={this.handleChange} />
                            </FloatingLabel>
                            <FloatingLabel
                                controlId="exp"
                                label="Experience"
                                className="mb-1"
                            >
                                <Form.Control
                                    type="number"
                                    placeholder="exp"
                                    value={this.state.dataCreate.exp}
                                    // id="dataCreate.email"
                                    onChange={this.handleChange} />
                            </FloatingLabel>
                            <div className='text-end'>
                                <Button variant="primary" onClick={this.handleSubmitCreate}>Create</Button>
                            </div>
                        </Card.Body>
                    </Card>

                    <Card className="card-list mx-2 my-5">
                        <Card.Body>
                            {/* <Card.Title>LIST PLAYER</Card.Title> */}
                            {/* <Card.Subtitle className="mb-2 text-muted">Tambahkan User</Card.Subtitle> */}
                            {/* <Card.Text>
                                Semua data palyer yang terdaftar.
                            </Card.Text> */}

                            <Form.Group className="mb-3" >
                                <Card className='p-3'>
                                    {/* <Form.Label>Search</Form.Label> */}
                                    <Card.Title>SEARCH</Card.Title>
                                    <Row>
                                        <div className='col-6 px-1 mb-2'>
                                            <Form.Control type="text" id="username" placeholder="Search Username" onChange={this.handleChangeSearch} />
                                        </div>
                                        <div className='col-6 px-1'>
                                            <Form.Control type="email" id="email" placeholder="Search E-mail" onChange={this.handleChangeSearch} />
                                        </div>
                                        <div className='col-6 px-1'>
                                            <Form.Control type="number" id="exp" placeholder="Search Experience" onChange={this.handleChangeSearch} />
                                        </div>
                                        <div className='col-6 px-1'>
                                            <Form.Control type="number" id="lvl" placeholder="Search Level" onChange={this.handleChangeSearch} />
                                        </div>
                                    </Row>
                                </Card>
                            </Form.Group>
                            <Card.Title>LIST PLAYER</Card.Title>
                            <CardPlayerTable players={this.state.players} getPlayerData={this.getPlayerData} />
                        </Card.Body>

                    </Card>
                </Row>


            </Container>
        );
    }
}


export default Home;
