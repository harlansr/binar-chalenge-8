import { Modal, Form, InputGroup, Button } from 'react-bootstrap';
import { Component } from "react";

class ModalPlayerEdit extends Component {
    state = {
        id: '',
        username: '',
        email: '',
        exp: '',
    }

    updateData = () => {
        this.setState({
            id: this.props.data.id,
            username: this.props.data.username,
            email: this.props.data.email,
            // exp: this.props.data.exp,
        })
    }

    handleSubmit = async () => {
        const data = {
            username: this.state.username,
            email: this.state.email,
            // exp: this.state.exp
        }

        const resp = await fetch(`http://localhost:5000/api/players/${this.state.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })

        if (resp.status === 200) {
            this.props.toggleFunc()
        }
    }

    handleChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        })
    }

    render() {
        return (
            <Modal show={this.props.showModal} onHide={this.props.toggleFunc} onShow={this.updateData}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Player</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <InputGroup className="mb-3">
                        <InputGroup.Text id="basic-addon1">Username</InputGroup.Text>
                        <Form.Control
                            placeholder="Username..."
                            aria-label="username"
                            aria-describedby="basic-addon1"
                            id="username"
                            value={this.state.username}
                            onChange={this.handleChange}
                        />
                    </InputGroup>
                    <InputGroup className="mb-3">
                        <InputGroup.Text id="basic-addon1">Email</InputGroup.Text>
                        <Form.Control
                            placeholder="Email..."
                            aria-label="Email"
                            aria-describedby="basic-addon1"
                            id="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </InputGroup>
                    {/* <InputGroup className="mb-3">
                        <InputGroup.Text id="basic-addon1">Experience</InputGroup.Text>
                        <Form.Control
                            placeholder="Experience..."
                            aria-label="Experience"
                            aria-describedby="basic-addon1"
                            id="exp"
                            value={this.state.exp}
                            onChange={this.handleChange}
                        />
                    </InputGroup> */}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success" onClick={this.handleSubmit}>
                        Submit
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default ModalPlayerEdit