import { Component } from "react";
import { Button, Table } from 'react-bootstrap';
import ModalPlayerEdit from '../components/ModalPlayerEdit'

class CardPlayerTable extends Component {
    state = {
        showModalEdit: false,
        dataEdit: {
            id: 0,
            username: '',
            email: '',
            exp: '',
        },
        dataDelete: {
            id: 0,
        },
    }

    toggleModalEditData = (id, username, email, exp) => {
        this.setState({
            dataEdit: {
                id,
                username,
                email,
                exp,
            },
            showModalEdit: !this.state.showModalEdit
        })
        this.props.getPlayerData()
    }

    toggleModalEdit = () => {
        this.setState({
            showModalEdit: !this.state.showModalEdit
        })
        this.props.getPlayerData()
    }

    handleDelete = async (id) => {
        console.log("Delete id=", id)
        const resp = await fetch(`http://localhost:5000/api/players/${id}`, {
            method: 'DELETE'
        })
        console.log(resp)
        if (resp.status === 200) {
            this.props.getPlayerData()
        }
    }

    render() {
        return (
            <div>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Username</th>
                            <th>E-mail</th>
                            <th className="text-center">Level</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.players.map((player, index) => (
                                <tr key={player.id}>
                                    <td>{index + 1}</td>
                                    <td>{player.username}</td>
                                    <td>{player.email}</td>
                                    <td className="text-center">{player.lvl}</td>
                                    <td className="text-end">
                                        <Button className='mx-1' onClick={() => { this.toggleModalEditData(player.id, player.username, player.email, player.exp) }}>Update</Button>
                                        <Button onClick={() => { if (window.confirm('Delete the item?')) { this.handleDelete(player.id) }; }}>Delete</Button>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>

                <ModalPlayerEdit
                    showModal={this.state.showModalEdit}
                    toggleFunc={this.toggleModalEdit}
                    data={this.state.dataEdit}
                />
            </div>

        )
    }
}


export default CardPlayerTable